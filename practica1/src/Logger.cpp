#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fstream>
#include <math.h>

#include "glut.h"
#define MAX 100

using namespace std;

int main (/*int argc, char* argv[]*/) {

	char cadena[MAX];
	
	mkfifo("/tmp/TuberiaLogger",0777);
	int fd = open("/tmp/TuberiaLogger",O_RDONLY);
	while(1)
	{
		char cadena[MAX];
		read(fd,cadena,sizeof(cadena));
		if (cadena[0]=='J')
		{
			printf("%s\n",cadena);
			sprintf(cadena,"");
		}
	}
	
	close(fd);
	
	unlink("/tmp/TuberiaLogger");

	return 0;
}

