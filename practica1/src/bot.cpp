#include <iostream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

#include "Esfera.h"
#include "DatosMemCompartida.h"

int main() 
{

	int file;
	char* org;
	DatosMemCompartida* punterodatos;

	file=open("/tmp/datos.txt",O_RDWR);
	org=(char*)mmap(NULL,sizeof(punterodatos),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
	close(file);
	punterodatos=(DatosMemCompartida*)org;

	float posicion;

	while(1)
	{
		posicion = (punterodatos->raqueta1.y2+punterodatos->raqueta1.y1)/2;
		if(posicion<punterodatos->esfera.centro.y)
		{
			punterodatos->accion=1;
		}
		else if(posicion>punterodatos->esfera.centro.y)
		{
			punterodatos->accion=-1;
		}
		else
		{
			punterodatos->accion=0;
			usleep(25000);
		}
	}
	
	munmap(org,sizeof(*(punterodatos)));
}

